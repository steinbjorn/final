from flask import Flask, jsonify, request
import requests
from pprint import pprint
import json
from decimal import *

from RestAI import RestAI

restAI = RestAI()

app = Flask(__name__)
authenticate_headers = {
    'content_type': 'application/x-www-form-urlencoded'
}

authenticate_body = {
    'client_secret': '75CDlnh4hYeYOzjez6FAt1Xhg1XU3pB4I7xebWv9irBUeaZLjl2X57X7zFqeRUNy',
    'client_id': 'ARpnqb1xUIgf-6g84AG9tg',
    'grant_type': 'client_credentials',
}

test_business = {
    'latitude': 36.1922841,
    'longitude': -115.1592718,
    'name': 'Cut and Taste'
}


def authenticate():
    return requests.post('https://api.yelp.com/oauth2/token', headers=authenticate_headers, data=authenticate_body)

auth_string = authenticate()
auth_string = auth_string.json()
auth_string = auth_string['access_token']

def generateSearchObject(business):
    return {
    'latitude': Decimal(business[0]['latitude']),
    'longitude': Decimal(business[0]['longitude']),
    'radius': 10,
    'term': business[0]['name'],
    'limit': 1 }

tasks = [
    {
        'id': 1,
        'title': u'Buy groceries',
        'description': u'Milk, Cheese, Pizza, Fruit, Tylenol',
        'done': False
    },
    {
        'id': 2,
        'title': u'Learn Python',
        'description': u'Need to find a good Python tutorial on the web',
        'done': False
    }
]

@app.route('/api/search', methods=['GET'])
def get_tasks():
    city = request.args.get('city')
    description = request.args.get('description')
    print 'RECOMMENDING'
    businesses = restAI.Recommend(description, city, 5)
    #pprint(auth_string)
    res = []
    for business in businesses:
        restaurant = requests.get('https://api.yelp.com/v3/businesses/search', headers={'Authorization': 'Bearer ' + auth_string }, params=generateSearchObject(business) )
        restaurant = restaurant.json()
        #pprint(restaurant)
        if len(restaurant['businesses']) > 0:
            res.append(restaurant['businesses'][0])
    #pprint(res)
    response = jsonify(res)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

if __name__ == '__main__':
    app.run(debug=True)
