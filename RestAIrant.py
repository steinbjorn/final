import sys, pprint, re, json, nltk, datetime
import numpy as np
from nltk.corpus import stopwords
from scipy.special import expit

from utilities import bcolors, parseReview
from dataProvider import DataProvider
from tfidf import tfidf

class RestAIrant():
        dataProvider = None
        tfidf = None
        inputObjects = None
        restaurants = None

        synapse_0 = None
        synapse_1 = None

        numberOfNeurons = None
        numberOfIterations = 1000
        iterations = 1000
        alpha = 0.02
        neurons = 40

        selfAware = True

        def __init__(self, dataProvider, tfidf, neurons=40, alpha=0.005, iterations=1000):
            np.random.seed(1)

            self.numberOfNeurons = neurons
            self.dataProvider = dataProvider
            self.tfidf = tfidf

            self.alpha = alpha
            self.numberOfIterations=iterations

            self.ReadInputData()
            #self.ReadTrainingData()

        def ReadInputData(self):
            self.inputObjects = self.dataProvider.getInputData()

        def ReadTrainingData(self):
            self.trainingObjects = self.dataProvider.getTrainingData()
            np.random.shuffle(self.trainingObjects)
            return self.trainingObjects

        def ReadSynapseData(self):
            synapse = self.dataProvider.getSynapseData()
            self.synapse_0 = np.asarray(synapse['synapse0'], dtype=np.float64)
            self.synapse_1 = np.asarray(synapse['synapse1'], dtype=np.float64)

        def GetNeuronCount(self, inputNeurons, outputNeurons, numberOfSamples):
            return int(numberOfSamples/(self.alpha*(inputNeurons + outputNeurons)))

        def NonLin(self, x, deriv=False):
            if(deriv == True):
                return x*(1.0-x)
            return expit(x)

        def Think(self, data):
            l0 = data
            l1 = self.NonLin(np.dot(l0, self.synapse_0))
            l2 = self.NonLin(np.dot(l1, self.synapse_1))

            return l2
        def EncodeCity(self, inp, inputCity):
            cities = self.dataProvider.getTopCities()
            for city in cities:
                inp.append(1) if inputCity == city else inp.append(0)
            return inp

        def LoadTrainingData(self):
            trainingRestaurants = self.dataProvider.getTrainingRestaurants()

            trainingData = []
            restaurantDict = {}

            restaurantIds = []
            restaurantCities = {}

            for restaurant in trainingRestaurants:
                restaurantIds.append(restaurant['business_id'])
                restaurantCities[restaurant['business_id']] = restaurant['city']

            #pprint.pprint(restaurantIds)
            #pprint.pprint(restaurantCities)

            print "Loading test data.."
            for index, restaurant in enumerate(trainingRestaurants):
                print "Loading restaurant " + str(index) + "/"+str(len(trainingRestaurants))
                output = []
                for ID in restaurantIds:
                    output.append(1) if ID == restaurant['business_id'] else output.append(0)
                restaurantDict[str(output)] = restaurant['name']

                for result in self.dataProvider.getTrainingReviews(restaurant['business_id'], percentage=.20, top=False): # self.reviews.find({"business_id": restaurant['business_id']}).skip(100).limit(3):
                    trainingData.append({'input': self.EncodeCity(list(self.tfidf.tfidf(result)), restaurant['city']), 'output': output})

            pprint.pprint(restaurantDict)
            with open("testData.json", "w") as td:
                json.dumps(restaurantDict, td, indent=4, sort_keys=True)
            print "Done"

            self.trainingData = trainingData
            self.restaurantDict = restaurantDict
            self.restaurantIds = restaurantIds
            self.restaurantCities = restaurantCities
            self.trainingRestaurants = trainingRestaurants
            self.trainingAccuracies = []

        def Classify(self):
            trainingRestaurants = self.trainingRestaurants

            trainingData = self.trainingData
            restaurantDict = self.restaurantDict

            restaurantIds = self.restaurantIds
            restaurantCities = self.restaurantCities

            n_correct = 0
            g_correct = 0
            n_incorrect = 0

            for train in trainingData:
                print "\n"
                output = self.Think(train['input'])
                t_data = np.asarray(train['output'])
                #pprint.pprint(output)
                #pprint.pprint(t_data)

                outputSorted = output.argsort()[-3:][::-1]
                firstGuess = [0] * len(output)
                secondGuess = [0] * len(output)
                thirdGuess = [0] * len(output)

                firstGuess[outputSorted[0]] = 1
                secondGuess[outputSorted[1]] = 1
                thirdGuess[outputSorted[2]] = 1

                #print "Restaurant: " + restaurantDict[str(t_data.tolist())]
                #'''print ("AI guess: \n\t#1 - " +
            #        restaurantDict[str(firstGuess)] + "(" + str(output[outputSorted[0]]) + ")\n\t#2 - " +
            #        restaurantDict[str(secondGuess)] + "(" + str(output[outputSorted[1]]) + ")\n\t#3 - " +
            #        restaurantDict[str(thirdGuess)] + "(" + str(output[outputSorted[2]]) + ")"
            #    )'''

                if(t_data.tolist() == firstGuess):
                    n_correct += 1
            #        print bcolors.OKGREEN + "Correct!" + bcolors.ENDC
                    #pprint.pprint(res)
                else:
                    if(t_data.tolist() == secondGuess):
            #            print bcolors.WARNING + "Second guess is correct!" + bcolors.ENDC
                        g_correct += 1
                    #else:
            #            print bcolors.FAIL + "Wrong.." + bcolors.ENDC
                    n_incorrect += 1

            accuracyFirst = str(n_correct * 100.0 / (n_correct + n_incorrect)) + "%"
            accuracySecond = str((n_correct + g_correct) * 100.0 / (n_correct + n_incorrect)) + "%"
            self.trainingAccuracies.append({ 'firstAccuracy': accuracyFirst, 'secondAccuracy': accuracySecond, 'iterations': self.iterations, 'alpha': self.alpha, 'neurons': self.neurons})
            return (n_correct * 100.0 / (n_correct + n_incorrect))

        def PrintResults(self):
            for trainingAccuracy in self.trainingAccuracies:
                print 'NEURONS: ' + str(trainingAccuracy['neurons']) + ', ALPHA: ' + str(trainingAccuracy['alpha']) + ', ITERATIONS: ' + str(trainingAccuracy['iterations'])
                print 'FIRST ACCURACY: ' + trainingAccuracy['firstAccuracy']
                print 'SECOND ACCURACY: ' + trainingAccuracy['secondAccuracy']

        def SaveSynapseData(self, syn0, syn1):
            synapse = {'synapse0': syn0.tolist(), 'synapse1': syn1.tolist()}
            self.dataProvider.setSynapseData(synapse)

        def Train(self, iters = 1000, alphaVal = 0.00005, neurons=40):
            self.iterations = iters
            self.alpha = alphaVal
            self.neurons = neurons
            inputArray = []
            outputArray = []
            trainingObjects = self.ReadTrainingData()
            count = 0

            for trainingObject in trainingObjects:
                count += 1
                inputArray.append(trainingObject['input'])
                outputArray.append(trainingObject['output'])

            objLen = trainingObjects[0]

            x = np.array(inputArray, dtype=np.float64)
            y = np.array(outputArray, dtype=np.float64)

            inputNeurons = len(objLen['input'])
            outputNeurons = len(objLen['output'])

            print "Input neurons: " + str(inputNeurons)
            print "Output neurons: " + str(outputNeurons)

            self.numberOfIterations = iters
            self.alpha = alphaVal #0.2, 0.00005

            #neuronCount = self.GetNeuronCount(inputNeurons, outputNeurons, count)
            neuronCount = self.numberOfNeurons
            neuronCount = neurons #23, 40

            syn0 = 2*np.random.random((inputNeurons, neuronCount)) - 1
            syn1 = 2*np.random.random((neuronCount, outputNeurons)) - 1

            print "Running " + str(self.numberOfIterations) + " iterations, using " + str(neuronCount)+ " neurons. Alpha: " + str(self.alpha)

            for j in xrange(self.numberOfIterations):
                l0 = x

                l1 = self.NonLin(np.dot(l0, syn0))
                l2 = self.NonLin(np.dot(l1, syn1))

                l2_error = y - l2
                if (j % 10) == 0:
                    print "Iteration " + str(j) + "/" +str(self.numberOfIterations) + "@"+ str(datetime.datetime.now())
                    #self.SaveSynapseData(syn0, syn1)
                    print "Error:" + str(np.mean(np.abs(l2_error)))

                l2_delta = l2_error* self.NonLin(l2, deriv=True)

                l1_error = l2_delta.dot(syn1.T)

                l1_delta = l1_error * self.NonLin(l1, deriv=True)

                syn1 += self.alpha * l1.T.dot(l2_delta)
                syn0 += self.alpha * l0.T.dot(l1_delta)

            self.SaveSynapseData(syn0, syn1)

#self, dataProvider = DataProvider(), tfidf=tfidf(), neurons=40, alpha=0.005, iterations=1000
dp = DataProvider()
tfidf = tfidf(dp)

restAI = RestAIrant(dp, tfidf, 40, 0.005, 1000)
inpt = raw_input("Retrain neural network (y/n)?\n")

if(inpt == 'y'):
    restAI.LoadTrainingData()
    restAI.Train(20000, 0.00005, 20)
    restAI.ReadSynapseData()
    restAI.Classify()
    restAI.PrintResults()
# To train the network run the functions:
# LoadTrainingData once in the beginning,
# Loop through Train, ReadSynapseData, Classify as often as you like
# Run PrintResults in the end to see the results

#    print "Training.."
#    restAI.Train()
#    print "Done!"

#print "Loading Synapse data"
#restAI.ReadSynapseData()
#print "Classifing test data"
#restAI.Classify()
'''
q = 'y'

while q != 'q':
    inpt = raw_input("FEED ME REVIEWS\n> ")
    if(inpt != 'q'):
        print inpt
        restAI.Review(inpt)
'''
