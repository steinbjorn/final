import pprint, pymongo
from pymongo import MongoClient
import collections
import random
from datetime import datetime
from bson.son import SON

#client = MongoClient()
#client = MongoClient('localhost', 27017)
client = MongoClient('mongodb://127.0.0.1:27017/')

db = client.arti

reviews = db.reviews
restaurants = db.business

class DbConnector:
    def __init__(self):
        self.trainingRestaurants = None
        self.numOfRestaurants = 0

    def getRestaurantByName(self, name):
        return list(restaurants.find({"name": name}))
    def getRestaurantById(self, name):
        return list(restaurants.find({"business_id": str(name)}))

    # Get restaurants we are training on
    def getTrainingRestaurants(self, numOfRestaurants = 10):
        if self.trainingRestaurants == None or self.numOfRestaurants != numOfRestaurants:
            self.numOfRestaurants = numOfRestaurants
            self.trainingRestaurants = list(restaurants.find({'categories': 'Restaurants'}).sort('review_count', pymongo.DESCENDING).limit(numOfRestaurants))
        return self.trainingRestaurants
    # get restaurant reviews by restaurant id
    def getRestaurantReviews(self, restaurantId, limit = None, skip = None):
        if limit == None:
            return list(reviews.find({"business_id": restaurantId}))
        if skip == None:
            return list(reviews.find({"business_id": restaurantId})).limit(limit)
        return list(reviews.find({"business_id": restaurantId}).skip(skip).limit(limit))

    def getTopCities(self, limit=3):
        pipeline = [
            { "$match" : { "categories" : "Restaurants" } },
            { "$group" : { "_id": '$city', "count": { "$sum": 1 } } },
            { "$sort"  : { "count": -1 } },
            { "$limit": int(limit)}
        ]
        return list(restaurants.aggregate(pipeline))

    def getTopReviewed(self, city, limit=3):
        return list(restaurants.find({'categories': 'Restaurants', "city": str(city)}).sort('review_count', pymongo.DESCENDING).limit(limit))

    def getLongestReviews(self, restaurantId, limit=200):
        pipeline = [
            { "$match": { "business_id": str(restaurantId) } },
            { "$project" : {
                 "byteLength": {"$strLenBytes": "$text"},
                 "review_id": "$review_id",
                 "text": "$text"
                 }
             },
             #{ $group: { _id: null, count: { $sum: 1 } }},
             { "$sort" : { "byteLength" : -1 }},
             { "$limit" : int(limit)},
             #{ "explain": True}
        ]
        #res =  list(db.command('aggregate', 'reviews', pipeline=pipeline, explain=True))
        return list(reviews.aggregate(pipeline))
        #pprint.pprint(res)
        #return res


#connector = DbConnector()
#pprint.pprint(connector.getRestaurantReviews('ujHiaprwCQ5ewziu0Vi9rw'))
