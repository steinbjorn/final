import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';

function mainReducer (state = { loading: false }, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default combineReducers({
  routing: routerReducer,
  main: mainReducer,
});
