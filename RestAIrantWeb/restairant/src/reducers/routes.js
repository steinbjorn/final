import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import {
  Route,
  IndexRoute,
  Router,
  browserHistory,
} from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import App from '../containers/App';
import MainPage from '../containers/MainPage';

const Routes = (props) => {
  const {
    store,
  } = props;
  const history = syncHistoryWithStore(browserHistory, store);
  return (
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={MainPage} />
    </Route>
  </Router>
  );
};

export default connect()(Routes);
