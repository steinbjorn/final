import React, { PropTypes, Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
// Necessary import for material ui
import injectTapEventPlugin from 'react-tap-event-plugin';
import { connect } from 'react-redux';
import { Link } from 'react-router';

injectTapEventPlugin();

class App extends Component {

  render() {
    const { children } = this.props;
    return (
      <MuiThemeProvider>
        <div>
          {children}
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: React.PropTypes.oneOfType([
    PropTypes.arrayOf(React.PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default connect()(App);
