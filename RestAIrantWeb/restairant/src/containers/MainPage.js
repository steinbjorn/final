import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import Map, { GoogleApiWrapper, InfoWindow } from 'google-maps-react';
import Marker from './Marker';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import {List, ListItem} from 'material-ui/List';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    height: 450,
    overflowY: 'auto',
  },
};


const renderBusiness = business => {
  const redirectToRestaurant = url => {
    window.open(url, '_blank')
  }
  return (
    <Card key={"card-" + business.id} style={{ width: '100%' }}>
        <CardTitle  title={business.name} style={{ marginBottom: 40 }}/>
        <CardMedia
          overlay={
            <div
              style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}
            >
              <ListItem
                disabled
                primaryText={business.display_phone}
                secondaryText={<span style={{ color: 'rgba(150, 150, 150, 0.9)' }}>Telephone</span>}
                style={{ color: '#FFF' }}
              />
              <ListItem
                disabled
                primaryText={business.is_closed ? 'CLOSED' : 'OPEN'}
                style={{ color: '#FFF' }}
              />
              {business.price &&
                <ListItem
                  disabled
                  primaryText={business.price}
                  secondaryText={<span style={{ color: 'rgba(150, 150, 150, 0.9)' }}>Price</span>}
                  style={{ color: '#FFF' }}
                />
              }
              <ListItem
                disabled
                primaryText={business.rating}
                secondaryText={<span style={{ color: 'rgba(150, 150, 150, 0.9)' }}>Rating</span>}
                style={{ color: '#FFF' }}
              />
              <ListItem
                disabled
                primaryText={business.location.display_address[0]}
                secondaryText={<span style={{ color: 'rgba(150, 150, 150, 0.9)' }}>{business.location.display_address[1]}</span>}
                style={{ color: '#FFF' }}
              />
            </div>
          }
        >
          <img src={business.image_url} style={{ width: '100%' }} />
        </CardMedia>
        <CardActions>
          <FlatButton label="Visit their homepage" onClick={() => redirectToRestaurant(business.url)} />
        </CardActions>
      </Card>
  );
}


class MainPage extends Component {
  state = {
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {},
    loading: false,
    loaded: false,
  };
  handleCityChange = (ev, index, value) => {
    this.setState({ city: value });
  }
  handleDescriptionChange = (ev, value) => {
    this.setState({ description: value })
  }
  onMapClicked = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };
  onMarkerClick = (props, marker, e) => {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  };

  handleSearch = () => {
    this.setState({ loading: true})
    return fetch(`http://localhost:5000/api/search?city=${this.state.city}&description=${this.state.description}`,
      {
        method: "GET",
      }).then(res => { return res.json();})
      .then(res => {
        this.setState({
          businesses: res,
          loading: false,
          loaded: true
        })
        console.log(this.state);
      })
    }

  render() {
    const getMarkers = () => {
      return this.state.businesses.map(business => {
        console.log(business);
        return (
          <Marker
            business={business}
            key={business.image_url}
            position={{ lat: business.coordinates.latitude, lng: business.coordinates.longitude }}
            onClick={this.onMarkerClick}
          />
        );
      });
    }
    const { selectedPlace } = this.state;
    return (
      <div>
        <AppBar title="Restaurant finder" showMenuIconButton={false} style={{ marginBottom: 32 }}/>
        <Paper style={{ margin: 32, padding: '16px 32px 16px 32px' }}>
          <Grid fluid style={{ padding: 32 }}>
            <Row>
              <Col
                md={12}
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  flexDirection: 'column',
                  alignItems: 'center',
                  width: '100%'
                }}>
                <h1>Search for restaurants</h1>
                <div style={{ width: '80%' }}>
                  <TextField
                    floatingLabelText="What kind of restaurant are you looking for?"
                    hintText="Restaurant search"
                    multiLine
                    fullWidth
                    rows={2}
                    rowsMax={10}
                    onChange={this.handleDescriptionChange}
                  />
                  <SelectField
                    floatingLabelText="Which city would you like to search for a restaurant in?"
                    fullWidth
                    onChange={this.handleCityChange}
                    value={this.state.city}
                  >
                    <MenuItem value="Las Vegas" primaryText="Las Vegas" />
                    <MenuItem value="Toronto" primaryText="Toronto" />
                    <MenuItem value="Phoenix" primaryText="Phoenix" />
                  </SelectField>
                  <RaisedButton primary style={{ width: '100%', marginTop: 16 }} onClick={this.handleSearch}>
                    <span style={{ color: '#FFF' }}>
                      Search
                    </span>
                  </RaisedButton>
                </div>
              </Col>
            </Row>
          </Grid>
        </Paper>
        {this.state.loaded && !this.state.loading &&
          <div>
            <Grid fluid style={{ padding: 32 }}>
              <Row>
                <Col md={12}>
                  <div style={styles.root}>
                    {this.state.businesses.map(renderBusiness)}
                  </div>
                </Col>
              </Row>
            </Grid>
            <Paper style={{ margin: 32, padding: '16px 32px 16px 32px', overflow: 'hidden' }}>
              <CardTitle title="See the restaurants on a map" />
              <Grid fluid style={{ padding: 32 }}>
                <Row>
                  <Col
                    md={12}
                    style={{ overflow: 'hidden' }}
                    >
                      <Map google={this.props.google}
                        style={{ width: '100%', height: '100%', position: 'relative' }}
                        className={'map'}
                        zoom={12}
                        initialCenter={{lat: this.state.businesses[0].coordinates.latitude, lng: this.state.businesses[0].coordinates.longitude }}
                      >
                        {getMarkers()}
                        <InfoWindow
                          marker={this.state.activeMarker}
                          visible={this.state.showingInfoWindow}
                          onClose={this.onInfoWindowClose}>
                          {selectedPlace.business &&
                            <div>
                              <h2>
                                <a href={selectedPlace.business.url}>
                                  {selectedPlace.business.name}
                                </a>
                              </h2>
                              <img src={selectedPlace.business.image_url} style={{ maxHeight: 150, maxWidth: 150 }} />
                              <h3>{selectedPlace.business.is_closed ? 'CLOSED' : 'OPEN'}</h3>
                              <h3>{selectedPlace.business.location.display_address[0]}</h3>
                              <h3>{selectedPlace.business.location.display_address[1]}</h3>
                            </div>
                          }
                        </InfoWindow>
                      </Map>
                    </Col>
                  </Row>
                </Grid>
              </Paper>
            </div>
        }
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBtOZpd-5alhJ6Iv-w7Hf1HAFE9WAdl-EY',
  libraries: ['places','visualization'],
  version: '3.27'
})(MainPage);
