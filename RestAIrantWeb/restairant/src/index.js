import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import './index.css';
import reducer from './reducers';
import Routes from './reducers/routes';

const store = createStore(
  reducer,
  applyMiddleware(thunk, createLogger({ collapsed: true })),
);

ReactDOM.render(
  <Provider store={store}>
    <Routes store={store} />
  </Provider>,
  document.getElementById('root'),
);
