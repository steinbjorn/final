import nltk
from nltk.stem.lancaster import LancasterStemmer
from nltk.corpus import stopwords # Import the stop word list
import re
stemmer = LancasterStemmer()
# Tokenize and stem a string into a list of tokens (words)
def parseReview(reviewString):
    letters_only = re.sub('[^a-zA-Z]',           # The pattern to search for
    ' ',                   # The pattern to replace it with
    reviewString)  # The text to search
    lower_case = letters_only.lower()        # Convert to lower case
    words = lower_case.split()               # Split into words
    words = [stemmer.stem(w) for w in words if not w in stopwords.words("english")]
    return words

def encodeCity2(reviewArray, cities, inputCity):
    #cities = [u'Phoenix', u'Las Vegas', u'Toronto']
    for city in cities:
        reviewArray.append(1) if inputCity == city else reviewArray.append(0)
    return reviewArray
def encodeRestaurants(restaurants):
    res = {}
    index = 0
    for restaurant in restaurants:
        encoded = [0] * len(restaurants)
        encoded[index] = 1
        res[str(encoded)] = restaurant
        index += 1
    return res

def encodeRestaurant(restaurantid, restaurantIds):
    output = []
    for ID in restaurantIds:
        output.append(1) if ID == restaurantid else output.append(0)
    return output
def encodeCity(restaurantCities, resturantID, cities):
    output = []
    for city in cities:
        output.append(1) if restaurantCities[resturantID] == city else output.append(0)
    return output

def citiesToList(cities):
    res = []
    for city in cities:
        res.append(city['_id'])
    return res
#def decodeRestaurant(restaurantMatrix, restaurants):

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
