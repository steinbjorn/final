import sys, pprint, re, json
import numpy as np
from scipy.special import expit
from pprint import pprint

import utilities as utils
from dataProvider import DataProvider
from tfidf import tfidf

class RestAI():

    synapse_0 = None
    synapse_1 = None

    tfidf = None
    dataProvider = None
    cities = None
    restaurants = None

    def __init__(self):
        np.random.seed(1)

        self.dataProvider = DataProvider()
        self.tfidf = tfidf()

        #Dictionary of all available restaurants with the restaurant featurematrix as a key
        #self.resturants[0,0,0,1,0,0] => "Some restaurantname"
        self.restaurants = self.dataProvider.getResturantEncDict()
        #List of the name of the available cities
        self.cities = self.dataProvider.getCitiesEncDict()
        #Read synapse data into memory
        self.ReadSynapseData()

    #Sigmoid function
    def NonLin(self, x, deriv=False):
        if(deriv == True):
            return x*(1.0-x)
        return expit(x)

    #Run our matrix through our trained neural network.
    #This returns a list of restaurants with a count of limitTop
    def Think(self, data, limitTop):
        l0 = data

        l1 = self.NonLin(np.dot(l0, self.synapse_0))
        l2 = self.NonLin(np.dot(l1, self.synapse_1))

        topResults = l2.argsort()[-limitTop:][::-1]
        results = [[0]*len(l2) for _ in range(len(topResults))]
        restaurants = []
        for index, topres in enumerate(topResults):
            results[index][topres] = 1
            restaurants.append(self.dataProvider.getRestaurant(self.restaurants[str(results[index])]))
        return restaurants

    #Read the our trained synapses from file
    def ReadSynapseData(self):
        synapse = self.dataProvider.getSynapseData()
        self.synapse_0 = np.asarray(synapse['synapse0'], dtype=np.float64)
        self.synapse_1 = np.asarray(synapse['synapse1'], dtype=np.float64)

    #This function accepts a search query for a restaurant and the city where the restaurant is located.
    #The searchQuery is transformed into a tfidf weighted feature matrix and the city is encoded and appended to that matrix.
    #The we run the matrix throught the neural network and get a result
    def Recommend(self, searchQuery, city, limit):
        featureMatrix = list(self.tfidf.tfidf(str(searchQuery)))
        featureMatrix = featureMatrix + self.cities[city]
        return self.Think(featureMatrix, limit)

#rest = RestAI()
#print rest.Recommend("porn", "Toronto", 10)
