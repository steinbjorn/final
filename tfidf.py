from __future__ import division
from pprint import pprint
from numpy import log, eye, dot
from collections import Counter, defaultdict
import numpy as np

from dataProvider import DataProvider
from utilities import parseReview

def normalized(a, axis=-1, order=2):
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2==0] = 1
    return a / np.expand_dims(l2, axis)

class tfidf:
    def __init__(self, dataProvider=DataProvider()):
        self.dataProvider = dataProvider
        self.inputData = self.dataProvider.getInputData()
        print len(self.inputData)
        self.idfMatrix = self.listToIdentityMatrix([self.idf(i) for i in self.inputData['wordList']])
    def getInputData(self):
        return self.inputData
    def getIdfMatrix(self):
        return self.idfMatrix
    def idf(self, word):
        a = self.inputData['totalReviews'] / (1 + self.inputData['wordDict'][word])
        return log(a)
    def listToIdentityMatrix(self, l):
        print len(l)
        identity = eye(len(l))
        return identity * l
    def tf(self, reviews):
        returnList = []
        for review in reviews:
            wordCount = defaultdict(int, Counter(parseReview(review)))
            reviewMatrix = []
            for word in self.inputData['wordList']:
                reviewMatrix.append(wordCount[word])
            returnList.append(reviewMatrix)
        return returnList
    def tfidfMulti(self, reviews):
        return normalized(dot(self.tf(reviews), self.idfMatrix))
    def tfidf(self, review):
        lis = []
        lis.append(review)
        return list(normalized(dot(self.tf(lis), self.idfMatrix))[0])


#wordList = tfi.getInputData()['wordList']
#wordDict = tfi.getInputData()['wordDict']
#pprint(wordList[0])
#pprint(tfi.idf(wordList[0]))
#pprint(wordDict[wordList[0]])
#pprint(wordList[1])
#pprint(tfi.idf(wordList[1]))
#pprint(wordDict[wordList[1]])
#pprint(wordList[-1])
#pprint(tfi.idf(wordList[-1]))
#pprint(wordDict[wordList[-1]])
#pprint(wordList[-2])
#pprint(tfi.idf(wordList[-2]))
#pprint(wordDict[wordList[-2]])

#testReview = ['yellow four this is a test to see something lobst craft', 'yellow four this is another test review lobst craft']

#print tfi.idf('monday')
#testInput = [[0.5, 2, 3, 0, 1, 0], [0, 1, 0, 1, 1, 0]]
#testList = [1, 1, 2, 4, 8, 12]
#testMatrix = tfi.listToIdentityMatrix(testList)
#testOutput = dot(testInput, testMatrix)
#print testOutput
