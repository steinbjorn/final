import json

class FileConnector:
    synapse_file = 'synapses.json'
    training_data_file = 'trainingData.json'
    input_data_file = 'inputData.json'
    info_file = 'info.json'
    restaurant_file = 'restaurants.json'
    top_cities = 'cities.json'

    def getTrainingData(self):
        with open(self.training_data_file) as data_file:
            return json.load(data_file)
    def setTrainingData(self, inp):
        with open(self.training_data_file, 'w') as outfile:
            json.dump(inp, outfile, indent=4, sort_keys=True)
    def getInputData(self):
        with open(self.input_data_file) as outputFile:
            return json.load(outputFile)
    def setInputData(self, inp):
        with open(self.input_data_file, 'w') as outfile:
            json.dump(inp, outfile, indent=4, sort_keys=True)
    def setRestaurantTrainingData(self, inp):
        with open(self.restaurant_file, 'w') as outfile:
            json.dump(inp, outfile, indent=4, sort_keys=True)
    def setInfoData(self, inp):
        with open(self.info_file, 'w') as outfile:
            json.dump(inp, outfile, indent=4, sort_keys=True)
    def setTopCities(self, inp):
        with open(self.top_cities, 'w') as outfile:
            json.dump(inp, outfile, indent=4, sort_keys=True)
    def getSynapseData(self):
        with open(self.synapse_file) as df:
            return json.load(df)
    def setSynapseData(self, synapse):
        with open(self.synapse_file, 'w') as outfile:
            json.dump(synapse, outfile, indent=4, sort_keys=True)
        #print 'saved synapses to: ' +  self.synapse_file
    def getResturantEncDict(self):
        with open(self.info_file) as df:
            return json.load(df)[0]
    def getCitiesEncDict(self):
        with open(self.info_file) as df:
            return json.load(df)[1]
