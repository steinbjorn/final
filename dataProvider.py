from dbConnector import DbConnector
from fileConnector import FileConnector

class DataProvider:
    dbConnector = DbConnector()
    fileConnector = FileConnector()

    def getTrainingRestaurants(self, numOfRestaurants = 15, numOfCities = 3):
        restaurants = []
        for city in self.getTopCities(numOfCities):
            restaurants = restaurants + self.getTopReviewed(city['_id'], numOfRestaurants)
        return restaurants
        #return self.dbConnector.getTrainingRestaurants(numOfRestaurants)
    def getRestaurantReviews(self, restaurantId, limit = None, skip = None):
        return self.dbConnector.getRestaurantReviews(restaurantId, limit, skip)
    def setInfo(self, data):
        self.fileConnector.setInfoData(data)
    def setTopCities(self, data):
        self.fileConnector.setTopCities(data)
    def setRestaurantTrainingData(self, data):
        self.fileConnector.setRestaurantTrainingData(data)
    def getTrainingData(self):
        return self.fileConnector.getTrainingData()
    def setTrainingData(self, inp):
        return self.fileConnector.setTrainingData(inp)
    def getInputData(self):
        return self.fileConnector.getInputData()
    def setInputData(self, inp):
        return self.fileConnector.setInputData(inp)
    def getSynapseData(self):
        return self.fileConnector.getSynapseData()
    def setSynapseData(self, synapse):
        self.fileConnector.setSynapseData(synapse)
    def getRestaurant(self, name):
        return self.dbConnector.getRestaurantById(name)
    def getRestaurants(self):
        trainingRestaurants = self.getTrainingRestaurants()
        restaurants = []
        for restaurant in self.getTrainingRestaurants():
            restaurants.append(restaurant['name'])
        return restaurants
    def getResturantEncDict(self):
        return self.fileConnector.getResturantEncDict()
    def getCitiesEncDict(self):
        return self.fileConnector.getCitiesEncDict()

    def setTopCities(self, limit=3):
        self.FileConnector.setTopCities(limit)
    def getTopCities(self, limit=3):
        top = self.dbConnector.getTopCities(limit)
        #self.setTopCities(top)
        return top

    def setTopReviewed(self, data):
        self.FileConnector.setTopReviews(data)
    def getTopReviewed(self, city, limit=3):
        top = self.dbConnector.getTopReviewed(city, limit)
        #self.setTopReviewed(top)
        return top

    def getLongestReviews(self, restaurantId, limit=200):
        return self.dbConnector.getLongestReviews(restaurantId, limit)
    def getTrainingReviews(self, resturantID, percentage=.80, top=True):
        reviews = self.getLongestReviews(resturantID, 200)
        returnReviews = []
        if top:
            for review in reviews[:int((len(reviews)+1)*percentage)]:
                returnReviews.append(review['text'])
        else:
            for review in reviews[int((len(reviews)+1)*percentage):]:
                returnReviews.append(review['text'])
        return returnReviews
