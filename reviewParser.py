from pprint import pprint
from collections import Counter

from tfidf import tfidf
from utilities import parseReview, encodeRestaurant, encodeCity
from dataProvider import DataProvider

testRestaurantReviews = [
'The quick review brown fox jumped over the lazy dog',
'This is a sample text for testing the tfidf functionality',
'This is a reviewing with the word dog in it',
'This Reviews has functional the word dog and the word fox in it']

class ReviewGenerator(object):
    def __init__(self, dataProvider = DataProvider(), tfidf=tfidf()):
        self.dataProvider = dataProvider
        self.trainingRestaurants = self.dataProvider.getTrainingRestaurants()
        self.tfidf = tfidf
        reviewList = []
        self.restaurantIds = []
        self.restaurantCities = {}

        #self.getTopCities()

        for restaurant in self.trainingRestaurants:
            self.restaurantIds.append(restaurant['business_id'])
            self.restaurantCities[restaurant['business_id']] = restaurant['city']

    # get the input set of the neural network
    # Returns:
    # A dictionary containing total number of reviews considered and a dictionary of
    # key value pairs of words with the words as keys and
    # the number of documents they appear in as values
    # example:
    # {
    #    'totalReviews': 100,
    #    'wordDict': { 'dog': 3, 'fox': 2 }
    #    'wordList': ['dog', 'fox']
    # }
    def getInputSet(self):
        returnList = []
        reviewList = []
        print 'Gathering reviews'
        for businessId in self.restaurantIds:
            print 'Reviews gathered: ' + str(len(reviewList))
            reviewList += self.getLongestReviews(businessId)
        print 'Finished gathering reviews, now Adding them to the input set'
        for review in reviewList:
            returnList += list(set(parseReview(review)))
        returnList = { k:v for k,v in dict(Counter(returnList)).items() if v > 4}
        print len(returnList)
        return dict({'totalReviews': len(reviewList), 'wordDict': returnList, 'wordList': list(returnList.keys())})

    def getLongestReviews(self, resturantID, percentage=.80, top=True):
        reviews = self.dataProvider.getLongestReviews(resturantID, 10)
        returnReviews = []
        if top:
            for review in reviews[:int((len(reviews)+1)*percentage)]:
                returnReviews.append(review['text'])
        else:
            for review in reviews[int((len(reviews)+1)*percentage):]:
                returnReviews.append(review['text'])
        return returnReviews

    def getRestaurantCities(self):
        return self.restaurantCities
    # Get limit number of reviews from each restaurant, optionally you can skip pages
    # This function returns a list of objects containing input and output data, example:
    # [ { 'output': [0,1,0,0], 'input': [1,0,0,0,1,1,0]}]
    def getTrainingData(self, limit=10, skips=0):
        returnList = []
        cities = [u'Phoenix', u'Las Vegas', u'Toronto']
        #cities = self.DataProvider.getTopCities(3)
        citiesDict = { }
        restaurantDict = { }
        allRestaurants = []
        for restID in self.restaurantIds:
            print 'Working on restaurant: ' + restID
            reviews = self.getLongestReviews(restID, .80, True)
            bottom = self.getLongestReviews(restID, .20, False)

            output = encodeRestaurant(restID, self.restaurantIds)
            restaurantDict[str(output)] = restID

            i = 0
            for review in reviews:
                i += 1
                if i % 100 == 0:
                    print 'Worked through ' + str(i) + ' reviews so far.'
                inp = list(self.tfidf.tfidf(review))
                cityEncoded = encodeCity(self.restaurantCities, restID, cities)
                citiesDict[self.restaurantCities[restID]] = cityEncoded
                inp = inp + cityEncoded

                returnList.append({'output': output, 'input': inp})

        self.dataProvider.setInfo([restaurantDict, citiesDict])
        self.dataProvider.setTrainingData(returnList)

#reviewGenerator = ReviewGenerator()
#dataProvider = DataProvider()
#dataProvider.setInputData(reviewGenerator.getInputSet())
